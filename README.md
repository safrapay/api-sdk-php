Biblioteca de integração Safrapay para PHP
===========================================

[![Minimum PHP Version](https://img.shields.io/badge/php-%3E%3D%207.1.3-blue.svg?style=flat-square)](https://php.net/)

Descrição
---------
Toda a implementação foi baseada na documentação da **Safrapay**, pelo link  [documentação api](https://sandbox.safrapay.com.br/#primeiros-passos).

 - [Criar Autorização de pagamento via cartão](https://sandbox.safrapay.com.br/#post-v2-charge-authorization)
 - [Criar Autorização de pagamento via boleto](https://sandbox.safrapay.com.br/#post-v2-charge-boleto)
 - [Consultar informação de um cobrança](https://sandbox.safrapay.com.br/#get-v2-charge-chargeid)

Requisitos
----------

 - [PHP] ^7.1.3
 - [cURL]
 - [Composer]

 Instalação
----------
> Nota: Recomendamos a instalação via **Composer**. Você também pode baixar o repositório como [arquivo zip] ou fazer um clone via Git.
 
 ### Instalação via Composer
> Para baixar e instalar o Composer no seu ambiente acesse https://getcomposer.org/download/ e caso tenha dúvidas de como utilizá-lo consulte a [documentação oficial do Composer].

É possível instalar a biblioteca safrapay-api-sdk-php via Composer da seguinte maneira:


- Adicionando a dependência ao seu arquivo ```composer.json```
  ```composer.json
  {
      "require": {
         "safrapay/safrapay-api-sdk-php" : "^1.1"
      }
  }
  ```
 
### Instalação manual
 - Baixe o repositório como [arquivo zip] ou faça um clone;
 - Descompacte os arquivos em seu computador;
 - Execute o comando ```php composer.phar install``` no local onde extraiu os arquivos.
 
 
 Como usar
 ---------
O diretório *[public](https://github.com/aditum-safra/safra-api-sdk-php/tree/master/publicpublic)* contém exemplos das mais diversas chamadas à API do Safrapay e o diretório *[src](https://github.com/aditum-safra/safra-api-sdk-php/tree/master/src)* contém a biblioteca propriamente dita (código fonte).

Seguir os passos a seguir:
- [Configuração do pacote](https://github.com/aditum-safra/safra-api-sdk-php/tree/master/documentCONFIG.md)
- [Pagamento por cartão](https://github.com/aditum-safra/safra-api-sdk-php/tree/master/documentPAYCC.md)
- [Pagamento por boleto](https://github.com/aditum-safra/safra-api-sdk-php/tree/master/documentPAYBOLETO.md)
- [Consultar transação](https://github.com/aditum-safra/safra-api-sdk-php/tree/master/documentPAYSTATUS.md)
- [Recursos de ajuda](https://github.com/aditum-safra/safra-api-sdk-php/tree/master/documentHELPER.md)
- [Enum](https://github.com/aditum-safra/safra-api-sdk-php/tree/master/documentENUM.md)


