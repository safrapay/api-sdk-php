# Configuration
Dentro dessa classe podemos definir e obter informações como a url de desenvolvimento e produção, merchanToken e CNPJ.

```php
Safrapay\ApiSDK\Configuration::initialize();

Safrapay\ApiSDK\Configuration::setUrl(Safrapay\ApiSDK\Configuration::DEV_URL); // Caso não defina a url, será usada de produção
Safrapay\ApiSDK\Configuration::setCnpj("83032272000109");
Safrapay\ApiSDK\Configuration::setMerchantToken("mk_P1kT7Rngif1Xuylw0z96k3");
Safrapay\ApiSDK\Configuration::setlog(true);

print_r(Safrapay\ApiSDK\Configuration::login());
```
