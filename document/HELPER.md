## Utils
Responsável por criar ter funcções de ajuda para a lib.

*billingInformation*

```php
Safrapay\ApiSDK\Configuration::initialize();
Safrapay\ApiSDK\Configuration::setUrl(Safrapay\ApiSDK\Configuration::DEV_URL);
Safrapay\ApiSDK\Configuration::setCnpj("83032272000109");
Safrapay\ApiSDK\Configuration::setMerchantToken("mk_P1kT7Rngif1Xuylw0z96k3");
Safrapay\ApiSDK\Configuration::setlog(true);
Safrapay\ApiSDK\Configuration::login();

$brandName = Safrapay\ApiSDK\Helper\Utils::getBrandCardBin("5463373320417272");
if ($brandName == NULL) {
    echo "Authorization::toJson = Falha ao buscar nome da bandeira do cartão\n";
    return NULL;
} else {
    echo "\n\n".$brandName."\n";
}
```
